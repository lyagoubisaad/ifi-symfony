# Plan  
cours : 9h-12h  
9->9:20 : presentation  
    9:20->9:30 : livecoding  
    9:30-10:10 : exercise  (Lucky coding - Palindrome)
    10:10-10:15 : correction  
    10:15:10:30:pause  
    10:30->11:45:Application   
    11:45:12h : correction de l'application  

## Livecoding  
    creation d'une page helloword (controller et vue)  
        route : config/routes.yaml  
        response : returning HTML from the controller  

# exercise  
    Application lucky number  
        number100  
            route : config/routes.yaml  
            response : returning HTML from the controller  
        number  
            argument  
            route : annotation  
                Install the annotations package  
                composer require annotations  
            response : render template  
            Installer le paquet de Twig  
            composer require twig   
    Palindrome             
                
## Application palindrome   
        
# Application  
### Creer un nouveau projet shopping   
    symfony new shopping  
Installez le support Doctrine via le pack orm Symfony, ainsi que le MakerBundle, qui vous aidera à générer du code  

```
composer require symfony/orm-pack  
composer require --dev symfony/maker-bundle
```
### Creer une base de donne  
    Dans le fichier .env ajouter la ligne ci-dessous
    DATABASE_URL=mysql://root:motdepassedurootmysql@127.0.0.1:3306/database_name?serverVersion=8.0  
    php bin/console doctrine:database:create  
### Creer entité article  
    php bin/console make:entity  
        Article   
            Nom : string 255 , required  
### Lier la Database  
    php bin/console make:migration   
    php bin/console doctrine:migrations:migrate  
### Creer articleForm  
    composer require form validator twig-bundle security  
    composer require symfony/form  
    php bin/console make:form  
### Modifiez article.php  
    ajouter dans le fichier /entity/article.php un atribut prix de type decimal  
    you can also use make:entity again  
### Modifiez articletype.php  
    realisez les changements necessaires dans le fichier article.php  
### Generate a new migration  
    La nouvelle propriété est cartographiée 'mapped', mais elle n'existe pas encore dans la table article  
    php bin/console make:migration  
    php bin/console doctrine:migrations:migrate  
### Modifiez article.php  
    ajouter dans le code un atribut quantité decimal    
    php bin/console make:entity --regenerate va generer les setters et getter 
### Creer un controller  
    php bin/console make:controller ArticleController  
    Implementez les methodes  
        Index  
        Afficher  
        Ajouter  
        Update
        Suprimer  
### Creer les templates  
    creer un dossier article dans templates  
    dedans creer les fichiers  
        index  
        edit  
        new  
        show  

### Ajouter bootsrap (Optionel)
    dans le fichier base.html.twig 
    {% block head_css %}
    <!-- Copy CSS from https://getbootstrap.com/docs/4.4/getting-started/introduction/#css -->
    {% endblock %}
    {% block head_js %}
    <!-- Copy JavaScript from https://getbootstrap.com/docs/4.4/getting-started/introduction/#js -->
    {% endblock %}

### Ajouter un filtre dans la barre Search ()
    Ajouter dans le controller une fonction search
    la route : article/name/{argument}
### Ajouter une relation onetomany (Category 1--N article) (en cours)
    Creer une entité category (relation OnetoMany avec article)
    Créez un CRUD pour l'entity Category
    Mettre a jour ArticleType
    en vous inspirant de la documentation       
        https://symfony.com/doc/current/reference/forms/types/entity.html#basic-usage   

    lier la base de donnée

### Lancer l'app
    symfony server:start        


    

