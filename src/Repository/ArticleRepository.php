<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    /**
     * @return Category[] Returns an array of Category objects
     */
    public function findByName(string $nom) {

        return $this->createQueryBuilder('article')
            ->where('article.Nom LIKE :nom')
            ->setParameter('nom', '%'.$nom.'%')
            ->orderBy('article.id', 'ASC')
            ->getQuery()
            ->execute()
            ;
    }
}
